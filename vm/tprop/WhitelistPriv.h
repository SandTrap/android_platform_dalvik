#ifndef _DALVIK_TPROP_WHITELIST_PRIV
#define _DALVIK_TPROP_WHITELIST_PRIV

#include "Dalvik.h"

// A whitelist of classes whose methods need not be emulated. The
// methods of these classes will have a simple taint prop logic where
// the return value's taint will be a union of the taints of the input
// arguments. The last entry in the array *must* be NULL.
static const char* const whitelist[] = {

    "android/animation/PropertyValuesHolder;",

    "android/content/res/AssetManager;",
    "android/content/res/StringBlock;",
    "android/content/res/XmlBlock;",

    "android/database/CursorWindow;",

    "android/graphics/Bitmap;", // TODO: Study this class for bytebuffer implications
    "android/graphics/BitmapFactory;", // TODO: Study this class for bytebuffer implications
    "android/graphics/BitmapShader;", // TODO: Study this class for bytebuffer implications
    "android/graphics/Camera;",
    "android/graphics/Canvas;",
    "android/graphics/ColorFilter;",
    "android/graphics/LinearGradient;",
    "android/graphics/Interpolator;",
    "android/graphics/Matrix;",
    "android/graphics/NinePatch;",
    "android/graphics/Paint;",
    "android/graphics/Path;",
    "android/graphics/Picture;",
    "android/graphics/PorterDuffColorFilter;",
    "android/graphics/PorterDuffXfermode;",
    "android/graphics/Region;",
    "android/graphics/Shader;",
    "android/graphics/SurfaceTexture;",
    "android/graphics/SweepGradient;",
    "android/graphics/Typeface;",
    "android/graphics/Xfermode;",

    "android/hardware/Camera;",
    "android/hardware/SystemSensorManager;",

    "android/media/ExifInterface;", // TODO: Study this class for bytebuffer implications
    "android/media/MediaPlayer;",

    "android/net/TrafficStats;",
    "android/net/LocalSocketImpl;",

    "android/opengl/ETC1;",
    "android/opengl/GLES10;",
    "android/opengl/GLES20;",
    "android/opengl/GLUtils;",
    "android/opengl/Matrix;",

    "android/os/Binder;",
    "android/os/BinderProxy;",
    "android/os/Debug;",
    "android/os/FileUtils;",
    "android/os/MessageQueue;",
    "android/os/Parcel;",
    "android/os/Process;",
    "android/os/StatFs;",
    "android/os/SystemClock;",
    "android/os/SystemProperties;",
    "android/os/Trace;",

    "android/text/AndroidBidi;",
    "android/text/format/Time;",

    "android/util/EventLog;",
    "android/util/FloatMath;",
    "android/util/Log;",

    "android/view/Display;",
    "android/view/DisplayEventReceiver;",
    "android/view/GLES20Canvas;",
    "android/view/GLES20DisplayList;",
    "android/view/HardwareRenderer;",
    "android/view/InputChannel;",
    "android/view/InputEventReceiver;",
    "android/view/MotionEvent;",
    "android/view/Surface;",
    "android/view/TextureView;",
    "android/view/VelocityTracker;",
    "android/view/KeyCharacterMap;",

    "com/android/internal/os/BinderInternal;",
    "com/android/internal/os/RuntimeInit;",

    "com/android/server/input/InputManagerService;",

    "com/google/android/gles_jni/EGLImpl;",

    "dalvik/system/NativeStart;", // TODO: We have to handle the effects of AttachVMThread here

    "java/io/File;",
    "java/io/ObjectStreamClass;",

    "java/lang/Character;",
    "java/lang/Math;",
    "java/lang/String;",
    "java/lang/StringToReal;",
    "java/lang/System;",

    "java/math/NativeBN;",

    "java/nio/charset/Charsets;",

    "java/util/regex/Matcher;",
    "java/util/regex/Pattern;",
    "java/util/zip/Deflater;", // TODO: Study this for byte buffer / byte array implications
    "java/util/zip/Inflater;", // TODO: Study this for byte buffer / byte array implications
    "java/util/zip/CRC32;", // TODO: Study this for byte buffer / byte array implications

    "libcore/icu/ICU;",
    "libcore/icu/NativeBreakIterator;",
    "libcore/icu/NativeConverter;",
    "libcore/icu/NativeDecimalFormat;",
    "libcore/io/AsynchronousCloseMonitor;",
    "libcore/io/Memory;",
    "libcore/io/Posix;",

    "org/apache/harmony/xnet/provider/jsse/NativeCrypto;",

    NULL};

/* function of type HashCompareFunc */
static int whitelistCmp(const void* ptr1, const void* ptr2)
{
    //ALOGI("comparing: %s and %s", ptr1, ptr2);
    return strncmp((char const*)ptr1, (char const*)ptr2, 128);
}

#endif
