#include "Dalvik.h"
#include "tprop/WhitelistPriv.h"
#include <anemu.h>
/* Wrapper to bundle a Field and an Object instance
 * - needed when dealing with nested instance field entries
 */
typedef struct {
    Field *field;
    Object *obj;
} FieldRef;

HashTable *gWhitelistTable = NULL;

#ifdef TAINT_JNI_LOG
/* JNI logging for debugging purposes
 * -- used to only print methods once (quiets things down a bit)
 */
HashTable *gJniLogSeen = NULL;
bool gJniLog = true;
#endif

int countWhitelistMethods(){
	int index = 0;
	int count = 0;

	while(whitelist[index++] != NULL){
		count++;
	}

	return count;

}
/* Code called from dvmJniStartup()
 * Initializes the gPolicyTable for fast lookup of taint policy
 * profiles for methods.
 */
void dvmTaintPropJniStartup()
{
    u4 hash;

    /* Create the policy table (perfect size) */
    int count = countWhitelistMethods();
    STLOGI("Counted whitelist methods: %d", count);

    gWhitelistTable = dvmHashTableCreate(
	    dvmHashSize(count),
	    NULL);
    dvmHashTableLock(gWhitelistTable);
    int index = 0;
    char * str;
    for (str = (char*)whitelist[index]; whitelist[index] != NULL; str = (char*)whitelist[++index]) {
    	STLOGI("inserting string %s from whitelist index %d", str, index);
	    hash = dvmComputeUtf8Hash(str);

	    STLOGI("%s hashed to %d", str, hash);
	    void* added = dvmHashTableLookup(gWhitelistTable, hash,(void *) str,
		    whitelistCmp, true);

	    STLOGI("added string: %s", added);
    }
    dvmHashTableUnlock(gWhitelistTable);

#ifdef TAINT_JNI_LOG
    /* JNI logging for debugging purposes */
    gJniLogSeen = dvmHashTableCreate(dvmHashSize(50), free);
#endif
}

bool isWhitelisted(const Method* method){

    // The format of this is "Ljava/lang/String;" if it's just a class
    // and "[Ljava/lang/String;" if it's an array.
    const char* className = method->clazz->descriptor;

    int startIndex = 0;
    if (className[0] == '[') {
        startIndex++; // skip the "["
    }
    startIndex++; // skip the "L"

    className += startIndex;

    u4 hash = dvmComputeUtf8Hash(className);

    // No need to lock the hashtable since this is read only
    void* itemFound = dvmHashTableLookup(gWhitelistTable, hash,
                                         (void*) className, whitelistCmp, false);

    if (itemFound == NULL) {
        return false;
    }

    return true;
}

/* Code called from dvmJniShutdown()
 * deallocates the gPolicyTable
 */
void dvmTaintPropJniShutdown()
{
    dvmHashTableFree(gWhitelistTable);
#ifdef TAINT_JNI_LOG
    /* JNI logging for debugging purposes */
    dvmHashTableFree(gJniLogSeen);
#endif
}

/* Returns the taint on an object.
 * - Currently only arrays and java.lang.String is supported
 */
u4 getObjectTaint(Object* obj, const char* descriptor)
{
    ArrayObject *arrObj = NULL;
    if (obj == NULL) {
	return TAINT_CLEAR;
    }

    if (descriptor[0] == '[') {
	/* Get the taint from the array */
	arrObj = (ArrayObject*) obj;
	if (arrObj != NULL) {
	    return arrObj->taint.tag;
	}
    }

    if (strcmp(descriptor, "Ljava/lang/String;") == 0) {
    StringObject * strObj = (StringObject*) obj;
	arrObj = strObj->array();
	if (arrObj != NULL) {
	    return arrObj->taint.tag;
	} /* else, empty string? don't worry about it */
    }

    /* TODO: What about classes derived from String? */

    /* Don't worry about other object types */
    return TAINT_CLEAR;
}

/* Sets the taint on the return value
 * - rtaint points to an address in the args array
 * - descriptor is the return type
 * - for return objects, only arrays and java.lang.String supported
 *   (will taint object reference returned otherwise)
 */
void setReturnTaint(u4 tag, u4* rtaint, JValue* pResult,
	const char* descriptor)
{
    Object* obj = NULL;
    ArrayObject* arrObj = NULL;

    //STLOGI("in setReturnTaint, ");
    switch (descriptor[0]) {
	case 'V':
	    /* void, do nothing */
	    break;
	case 'Z':
	case 'B':
	case 'C':
	case 'S':
	case 'I':
	case 'J':
	case 'F':
	case 'D':
	    /* Easy case */
	    *rtaint |= tag;
	    break;
	case '[':
	    /* Best we can do is taint the array, however
	     * this is not right for "[[" or "[L" */
	    arrObj = (ArrayObject*) pResult->l;
	    if (arrObj != NULL) {
		arrObj->taint.tag |= tag;
	    } /* else, method returning null pointer */
	    break;
	case 'L':
	    obj = (Object*) pResult->l;

	    if (obj != NULL) {
		if (strcmp(descriptor, "Ljava/lang/String;") == 0) {
		    arrObj = ((StringObject*) obj)->array();
		    if (arrObj != NULL) {
			arrObj->taint.tag |= tag;
		    } /* else, empty string?, don't worry about it */
		} else {
		    /* TODO: What about classes derived from String? */
		    /* Best we can do is to taint the object ref */
		    *rtaint |= tag;
		}
	    }
	    break;
    }
}

/* Returns the taint tag for a field
 * - obj only used if the field is not static
 */
u4 getTaintFromField(Field* field, Object* obj)
{
    u4 tag = TAINT_CLEAR;

    if (dvmIsStaticField(field)) {
		StaticField* sfield = (StaticField*) field;
		tag = dvmGetStaticFieldTaint(sfield);
    } else {
	InstField* ifield = (InstField*) field;
	if (field->signature[0] == 'J' || field->signature[0] == 'D') {
	    tag = dvmGetFieldTaintWide(obj, ifield->byteOffset);
	} else {
	    tag = dvmGetFieldTaint(obj, ifield->byteOffset);
	}
    }

    return tag;
}

/* add tag to a field
 * - obj only used if the field is not static
 */
void addTaintToField(Field* field, Object* obj, u4 tag)
{
    if (dvmIsStaticField(field)) {
	StaticField* sfield = (StaticField*) field;
	tag |= dvmGetStaticFieldTaint(sfield);
	dvmSetStaticFieldTaint(sfield, tag);
    } else {
	InstField* ifield = (InstField*) field;
	if (field->signature[0] == 'J' || field->signature[0] == 'D') {
	    tag |= dvmGetFieldTaintWide(obj, ifield->byteOffset);
	    dvmSetFieldTaintWide(obj, ifield->byteOffset, tag);
	} else {
	    tag |= dvmGetFieldTaint(obj, ifield->byteOffset);
	    dvmSetFieldTaint(obj, ifield->byteOffset, tag);
	}
    }
}

/* Returns the object pointer for a field
 * - obj only used if the field is not static
 * - Note: will not return an array object
 */
Object* getObjectFromField(Field* field, Object* obj)
{
    if (field->signature[0] != 'L') {
	return NULL;
    }

    if (dvmIsStaticField(field)) {
	StaticField* sfield = (StaticField*) field;
	return dvmGetStaticFieldObject(sfield);
    } else {
	InstField* ifield = (InstField*) field;
	return dvmGetFieldObject(obj, ifield->byteOffset);
    }
}

/* Returns the index in args[] corresponding to the parameter
 * string entry.
 * - It doesn't matter if the entry has multiple parts, e.g.,
 *   "param1.foo.bar", as long as the variable name after the first
 *   "." is not a number. Since the signature comes next, we can
 *   safely assume this is the case.
 * - returns -1 on parsing error
 * - If descriptor is not NULL, it will point to a newly allocated
 *   descriptor that needs to be free()'d (unless there was an error)
 */
int paramToArgIndex(const char* entry, const Method* method, char** descriptor) {
    int pIdx, aIdx, i;
    char* endptr;
    const char* num = entry + 5; /* "param" is the first 5 characters */
    const DexProto* proto = &method->prototype;
    DexParameterIterator pIterator;

    /* Step 1: determine the parameter index (pIdx) */
    pIdx = strtol(num, &endptr, 10);
    if (num == endptr) {
	/* error parsing */
	return -1;
    }

    /* Step 2: translate parameter index into args array index */
    dexParameterIteratorInit(&pIterator, proto);
    aIdx = (dvmIsStaticMethod(method)?0:1); /* index where params start */
    for (i=0; i<=pIdx ; i++) {
	const char* desc = dexParameterIteratorNextDescriptor(&pIterator);

	if (desc == NULL) {
	    /* This index doesn't exist, error */
	    return -1;
	}

	if (i == pIdx) {
	    /* This is the index */
	    if (descriptor != NULL) {
		*descriptor = strdup(desc);
	    }
	    break;
	}

	/* increment the args array index */
	aIdx++;

	if (desc[0] == 'J' || desc[0] == 'D') {
	    /* wide argument, increment index one more */
	    aIdx++;
	}
    }

    return aIdx;
}

/* Used to propagate taint for JNI methods
 * At the moment, only one type of propagation:
 *  -) simple conservative propagation based on parameters
 *
 * The following is not yet implemented:
 *  - propagation based on function profile policies
 */
void dvmTaintPropJniMethod(const u4* args, JValue* pResult, const Method* method)
{
    const DexProto* proto = &method->prototype;
    DexParameterIterator pIterator;
    int nParams = dexProtoGetParameterCount(proto);
    int pStart = (dvmIsStaticMethod(method)?0:1); /* index where params start */

    /* Consider 3 arguments. [x] indicates return taint index
     * 0 1 2 [3] 4 5 6
     */
    int nArgs = method->insSize;
    u4* rtaint = (u4*) &args[nArgs]; /* The return taint value */
    int tStart = nArgs+1; /* index of args[] where taint values start */
    int tEnd   = nArgs*2; /* index of args[] where taint values end */
    u4 tag = TAINT_CLEAR;
    int i;

    /* Union the taint tags, this includes object ref tags
     * - we don't need to worry about static vs. not static, because getting
     * the taint tag on the "this" object reference is a good
     * - we don't need to worry about wide registers, because the stack
     * interleaving of taint tags makes it transparent
     */
    for (i = tStart; i <= tEnd; i++) {
        tag |= args[i];
    }

    /* If not static, pull any taint from the "this" object */
    if (!dvmIsStaticMethod(method)) {
        tag |= getObjectTaint((Object*)args[0], method->clazz->descriptor);
    }

    /* Union taint from Objects we care about */
    dexParameterIteratorInit(&pIterator, proto);
    for (i=pStart; ; i++) {
        const char* desc = dexParameterIteratorNextDescriptor(&pIterator);

        if (desc == NULL) {
            break;
        }

        if (desc[0] == '[' || desc[0] == 'L') {
            tag |= getObjectTaint((Object*) args[i], desc);
        }

        if (desc[0] == 'J' || desc[0] == 'D') {
            /* wide argument, increment index one more */
            i++;
        }
    }

    /* Update return taint according to the return type */
    if (tag) {
        const char* desc = dexProtoGetReturnType(proto);
        setReturnTaint(tag, rtaint, pResult, desc);
    }
}


/* Check if JNI input arguments are tainted */
u4 dvmTaintCheckJniMethod(const u4* args, const Method* method, Thread* self)
{
    const DexProto* proto = &method->prototype;
    DexParameterIterator pIterator;
    int nParams = dexProtoGetParameterCount(proto);
    int pStart = (dvmIsStaticMethod(method)?0:1); /* index where params start */

    /* Consider 3 arguments. [x] indicates return taint index
     * 0 1 2 [3] 4 5 6
     */
    int nArgs = method->insSize;
    u4* rtaint = (u4*) &args[nArgs]; /* The return taint value */
    int tStart = nArgs+1; /* index of args[] where taint values start */
    int tEnd   = nArgs*2; /* index of args[] where taint values end */
    int tIndex = tStart + pStart;
    u4	tag = TAINT_CLEAR;
    u4  allTags = TAINT_CLEAR;
    int i;

    /* Union taint from arguments we care about */
    dexParameterIteratorInit(&pIterator, proto);
    for (i=pStart; ; i++) {
        const char* desc = dexParameterIteratorNextDescriptor(&pIterator);

        if (desc == NULL) {
            break;
        }

        tag = args[tIndex];
        allTags |= tag;

        if (desc[0] == 'I' || desc[0] == 'F' || desc[0] == 'B' ||
            desc[0] == 'C' || desc[0] == 'S' || desc[0] == 'Z' ||
            desc[0] == 'D' || desc[0] == 'J' || desc[0] == 'L') {

            /* Format Descriptors Glossary */
            // I = integer, F = float, B = byte,
            // C = character, S = short, Z = boolean
            // D = double, J = long
            // L = Obj. We taint the ptr here, to mirror the behavior of tdroid

            // We're only tainting references here.
            const uint32_t argRef = (uint32_t) &args[i];

            STLOGI("Tainting descriptor %s with tag %u", desc, tag);

#ifdef __arm__
            if (desc[0] == 'J' || desc[0] == 'D') {
                emu_set_taint_array(argRef, sizeof(uint32_t) * 2, tag);
            } else {
                emu_set_taint_array(argRef, sizeof(uint32_t), tag);
            }
#endif // __arm__

        } else if (desc[0] == '[') {

            Object *obj = (Object*) dvmDecodeIndirectRef(dvmThreadSelf(), (jobject)args[i]);
            u4 objTag = getObjectTaint(obj, desc);

            if (objTag != TAINT_CLEAR || tag != TAINT_CLEAR) {
                // No need to OR the reference tag. That's already done earlier in this method.
                allTags |= objTag;
            }
        }

        if (desc[0] == 'J' || desc[0] == 'D') {
            /* wide argument, increment index one more */
            i++;
            tIndex++;
        }

        tIndex++;
    }

    return allTags;
}

void dvmTaintReturn(const u4* args, JValue* pResult, const Method* method)
{
#ifdef __arm__
    const DexProto* proto = &method->prototype;
    const char* desc = dexProtoGetReturnType(proto);
    int nArgs = method->insSize;
    u4* rtaint = (u4*) &args[nArgs]; /* The return taint value */

    uint32_t tag = TAINT_CLEAR;
    uint32_t addr = (uint32_t) pResult;
    uint32_t length = sizeof(uint32_t);

    if (desc[0] == 'J' || desc[0] == 'L') {
        length = sizeof(uint32_t) * 2;
    }

    tag = emu_get_taint_array(addr, length);
    if (tag) {
        STLOGI("Tainting return value @ %p with tag: %d", addr, tag);
        setReturnTaint(tag, rtaint, pResult, desc);
    }
#endif // __arm__
}
