/*
 * Copyright (c) 2010 The Pennsylvania State University
 * Systems and Internet Infrastructure Security Laboratory
 *
 * Authors: William Enck <enck@cse.psu.edu>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include "ftaint.h"

#define USAGE_GET_FILE "Usage: %s g <file>: Get a file's taint label\n"
#define USAGE_SET_FILE "Usage: %s s <file> <hex>: Set a file's taint label\n"
#define USAGE_APPEND_FILE "Usage: %s a <file> <hex>: Append a taint label to a file\n"
#define USAGE_GET_DIR  "Usage: %s d <dir>: Get the taint labels of all the files in a directory\n"

void usage(const char *prog)
{
    fprintf(stdout, "Usage information printed to logcat\n");
    LOGI(USAGE_GET_FILE, prog);
    LOGI(USAGE_SET_FILE, prog);
    LOGI(USAGE_APPEND_FILE, prog);
    LOGI(USAGE_GET_DIR, prog);
    exit(1);
}

void printFileTaint(const char* filename) {
    u4 tag;
    char *ret = NULL;
    char abspath[2048];

    memset(abspath, 0, sizeof(abspath));
    ret = realpath(filename, abspath);
    if (ret == NULL) {
        LOGI("Cannot file real path for %s. Does the file exist?", filename);
    }

    tag = getTaintXattr(abspath);
    LOGI("File: %s, Taint: 0x%08x\n", abspath, tag);
}

int main(int argc, char *argv[])
{
    u4 tag;

    if (argc != 3 && argc != 4) {
        usage(argv[0]);
    }

    if (strlen(argv[1]) != 1) {
        usage(argv[0]);
    }

    fprintf(stdout, "Output will be printed to logcat\n");

    // Get the taint
    if (argc == 3) {
        if (argv[1][0] == 'g') {
            printFileTaint(argv[2]);
            return 0;
        } else if (argv[1][0] == 'd') {
            char *dirname = argv[2];
            char filename[2048];

            DIR *dir = opendir(dirname);
            if (dir == NULL) {
                LOGI("Could not open directory %s. Does it exist?", dirname);
                return 1;
            }

            struct dirent *ent = NULL;
            while ((ent = readdir(dir)) != NULL) {
                char *shortname = ent->d_name;
                if ((strcmp(shortname, ".") == 0) || (strcmp(shortname, "..") == 0)) {
                    continue;
                }

                memset(filename, 0, sizeof(filename));
                sprintf(filename, "%s/%s", dirname, shortname);
                printFileTaint(filename);
            }

            return 0;
        } else {
            usage(argv[0]);
        }
    }

    // Set the taint
    tag = strtol(argv[3], NULL, 16);
    if (tag == 0 && errno == ERANGE) {
        usage(argv[0]);
    }

    if (argv[1][0] == 's') {
        setTaintXattr(argv[2], tag);
    } else if (argv[1][0] == 'a') {
        u4 old = getTaintXattr(argv[2]);
        setTaintXattr(argv[2], tag | old);
    } else {
        usage(argv[0]);
    }

    return 0;
}
